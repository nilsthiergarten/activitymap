package de.cmlab.drm_app2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import de.cmlab.drm_app2.infrastructure.entity.NotificationReminderSetting;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;

public class SettingActivity extends AppCompatActivity {

    private String reminderTime = null;
    private NotificationReminderSetting notificationReminderSetting;
    private ActivityViewModel activityViewModel = new ActivityViewModel(getApplication());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        new Thread(() -> {
            notificationReminderSetting = activityViewModel.getReminderSetting();
            if (notificationReminderSetting == null) {
                notificationReminderSetting = new NotificationReminderSetting(null);
            } else {
                reminderTime = notificationReminderSetting.getTime();
            }

            runOnUiThread(this::refreshSetting);
        }).start();
    }

    public void setReminderTime(View view) {
        TimePickerDialog.OnTimeSetListener timePickerListener = (view1, hour, minute) -> {
            reminderTime = addZeroPrefixIfNecessaryAndGet(hour) + ":" + addZeroPrefixIfNecessaryAndGet(minute);
            refreshSetting();
        };

        createTimePickDialog(timePickerListener);
    }

    private void createTimePickDialog(TimePickerDialog.OnTimeSetListener eventHandler) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, eventHandler, 12, 0, true);
        timePickerDialog.show();

        int color = getResources().getColor(R.color.blue_500);
        timePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(color);
        timePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(color);
    }

    public void saveActivity(View view) {
        new Thread(() -> {
            Intent backToMain = new Intent(this, MainActivity.class);
            if (reminderTime != null && !reminderTime.equals(notificationReminderSetting.getTime())) {
                if (notificationReminderSetting.getTime() == null) {
                    notificationReminderSetting.setTime(reminderTime);
                    activityViewModel.insertReminderSetting(new NotificationReminderSetting(reminderTime));
                } else {
                    notificationReminderSetting.setTime(reminderTime);
                    activityViewModel.updateNotificationSetting(notificationReminderSetting);
                }
                runOnUiThread(() -> {
                    refreshSetting();
                    Toast.makeText(this, "Reminder stored to database", Toast.LENGTH_SHORT).show();
                });
            }

            runOnUiThread(() -> startActivity(backToMain));
        }).start();
    }

    private void refreshSetting() {
        Button reminderTimeButton = findViewById(R.id.activity_setting_activity_time_button);
        TextView settingTimeTextView = findViewById(R.id.activity_setting_reminder_text);
        settingTimeTextView.setVisibility(View.VISIBLE);

        settingTimeTextView.setOnClickListener(this::setReminderTime);
        if (reminderTime != null) {
            reminderTimeButton.setText("Click to Change rating reminder");
            settingTimeTextView.setText("Reminder at: " + reminderTime);
        } else {
            reminderTimeButton.setText("Click to set rating reminder");
            settingTimeTextView.setText("You have no reminder yet");
        }
        settingTimeTextView.setOnClickListener(this::setReminderTime);
    }

    private String addZeroPrefixIfNecessaryAndGet(int number){
        String timeValue = String.valueOf(number);
        return timeValue.length() == 1 ? "0".concat(timeValue) : timeValue ;
    }
}
