package de.cmlab.drm_app2.backgroundServices.gpsTracking;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.time.LocalDateTime;
import java.util.Optional;

import de.cmlab.drm_app2.backgroundServices.NotificationCreator;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorage;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorageImpl;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;
import de.cmlab.drm_app2.sensors.gps.PositionService;
import de.cmlab.drm_app2.sensors.gps.PositionServiceImpl;
import de.cmlab.drm_app2.sensors.gps.exceptions.NoGPSPermissionException;

public class LocationService extends Service {

    private static final InMemoryStorage inMemoryStorage = InMemoryStorageImpl.getInstance();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(NotificationCreator.getNotificationId(),
                NotificationCreator.getNotification(this));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Starting Simple Location");

        PositionService positionService = startGPSTracking();
        LocationAlgorithm locationAlgorithm = new LocationAlgorithm(positionService);

        new Thread(() -> {
            while (true) {
                try {
                    Location currentLocation = positionService.getCurrentLocationUsingGPS();
                    String streetAddress = positionService.getAddressUsingLocationAsString(currentLocation);

                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                   // NotificationCreator.updateNotificationWithCurrentAddress(notificationManager, this, streetAddress);

                    LocalDateTime localDateTime = LocalDateTime.now();
                    Optional<ActivityAndPlace> maybeFinishedActivity = locationAlgorithm.addPoint(currentLocation, localDateTime);

                    maybeFinishedActivity.ifPresent(ActivityViewModel.getInstance()::insertActivityWithPlace);

                   Thread.sleep(1000 * 60);
                } catch (Exception ignored) {
                }
            }
        }, "LocationService-Thread").start();
        return START_STICKY;
    }

    private PositionService startGPSTracking() {
        PositionService positionService = new PositionServiceImpl(this, new Geocoder(this), inMemoryStorage);
        try {
            positionService.startGPSTracking();
        } catch (NoGPSPermissionException ignored) {
        }
        return positionService;
    }
}
