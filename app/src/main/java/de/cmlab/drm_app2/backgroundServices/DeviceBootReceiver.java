package de.cmlab.drm_app2.backgroundServices;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import de.cmlab.drm_app2.backgroundServices.alarm.AlarmJobService;
import de.cmlab.drm_app2.backgroundServices.gpsTracking.LocationJobService;

public class DeviceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        scheduleJob(context, LocationJobService.class, LocationJobService.JOB_ID, 1, 1);
        scheduleJob(context, AlarmJobService.class, AlarmJobService.JOB_ID, 1, 1);
    }

    public static void scheduleJob(Context context, Class service, int jobId, int minDelayInSeconds, int maxDelayInSeconds) {
        ComponentName serviceComponent = new ComponentName(context, service);
        JobInfo.Builder builder = new JobInfo.Builder(jobId, serviceComponent);
        builder.setMinimumLatency(minDelayInSeconds * 1000);
        builder.setOverrideDeadline(maxDelayInSeconds * 1000);
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }
}