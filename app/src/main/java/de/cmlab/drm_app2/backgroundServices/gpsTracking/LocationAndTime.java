package de.cmlab.drm_app2.backgroundServices.gpsTracking;

import android.location.Location;

import java.time.LocalDateTime;
import java.util.Objects;

public class LocationAndTime {
    String address;
    Location location;
    public double longitude;
    public double latitude;
    LocalDateTime time;

    public LocationAndTime(String address, Location location, double longitude, double latitude, LocalDateTime time) {
        this.address = address;
        this.location = location;
        this.longitude = longitude;
        this.latitude = latitude;
        this.time = time;
    }

    public String getLocation() {
        return address;
    }

    public void setLocation(String address) {
        this.address = address;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationAndTime that = (LocationAndTime) o;
        return Objects.equals(address, that.address) &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, time);
    }
}
