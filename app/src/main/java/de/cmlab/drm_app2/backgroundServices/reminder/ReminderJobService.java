package de.cmlab.drm_app2.backgroundServices.reminder;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

public class ReminderJobService extends JobService {

    public static final int JOB_ID = 3;

    @Override
    public boolean onStartJob(JobParameters params) {
        Intent service = new Intent(getApplicationContext(), ReminderService.class);
        getApplicationContext().startForegroundService(service);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}
