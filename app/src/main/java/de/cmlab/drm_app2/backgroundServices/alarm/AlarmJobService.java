package de.cmlab.drm_app2.backgroundServices.alarm;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

public class AlarmJobService extends JobService {

    public static final int JOB_ID = 2;

    @Override
    public boolean onStartJob(JobParameters params) {
        Intent service = new Intent(getApplicationContext(), AlarmService.class);
        getApplicationContext().startForegroundService(service);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}
