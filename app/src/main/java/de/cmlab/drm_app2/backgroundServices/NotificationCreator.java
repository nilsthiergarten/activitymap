package de.cmlab.drm_app2.backgroundServices;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import de.cmlab.drm_app2.MainActivity;
import de.cmlab.drm_app2.R;

public class NotificationCreator {

    private static final int NOTIFICATION_ID = 1094;
    private static String CHANNEL_ID;
    private static Notification notification;

    public static Notification getNotification(Context context) {

        if (notification == null) {
            CHANNEL_ID = createChannelNotification(context);
            notification = new NotificationCompat.Builder(context, CHANNEL_ID).build();
        }

        return notification;
    }

    public static void updateNotificationWithCurrentMessage(NotificationManager notificationManager, Context context, String description) {

        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification newNotification = createBackgroundNotification(context)
                .setContentTitle("Rating Reminder")
                .setContentText(description)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(description))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        notificationManager.notify(NOTIFICATION_ID, newNotification);
    }

    private static NotificationCompat.Builder createBackgroundNotification(Context context) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID);
        return notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setCategory(Notification.CATEGORY_SERVICE);
    }

    public static int getNotificationId() {
        return NOTIFICATION_ID;
    }

    private synchronized static String createChannelNotification(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        String NOTIFICATION_CHANNEL_ID = "de.uni_bamberg.hci.drm_app2.jobService.services.locationService";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = "Location Background Service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
            chan.enableLights(true);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(chan);
        }

        return NOTIFICATION_CHANNEL_ID;
    }
}
