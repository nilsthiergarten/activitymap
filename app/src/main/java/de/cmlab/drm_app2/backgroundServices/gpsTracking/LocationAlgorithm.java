package de.cmlab.drm_app2.backgroundServices.gpsTracking;

import android.graphics.PointF;
import android.location.Location;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.cmlab.drm_app2.infrastructure.entity.Activity;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.Place;
import de.cmlab.drm_app2.sensors.gps.PositionService;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownAddressesException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownLastLocationException;

public class LocationAlgorithm {

    protected PositionService positionService;

    // protected -> We are able to manipulate the cache for testing by inheritance
    protected static List<LocationAndTime> activityCache;

    // protected -> Used for testing
    protected LocationAlgorithm() {

    }

    public LocationAlgorithm(PositionService positionService) {
        this.positionService = positionService;
    }

    public Optional<ActivityAndPlace> addPoint(Location location, LocalDateTime time) throws UnknownAddressesException, UnknownLastLocationException, IOException {
        String address = positionService.getAddressUsingLocationAsString(location);

        if (activityCache == null || activityCache.isEmpty()) {
            activityCache = new ArrayList<>();
            activityCache.add(new LocationAndTime(address, location, location.getLongitude(), location.getLatitude(), time));
            return Optional.empty();
        }

        boolean isSameActivity = IsSameActivity(address, location);

        if (isSameActivity) {
            if (activityCache.size() == 2) {
                activityCache.remove(1);
            }

            activityCache.add(new LocationAndTime(address, location, location.getLongitude(), location.getLatitude(), time));
            return Optional.empty();
        } else if (activityCache.size() == 2) {
            // we went away from the last activity location (isSameActivity == false)
            // hence, the activity ended, so we return it

            String startDate = activityCache.stream().map(x -> x.time).min(LocalDateTime::compareTo).get().toString();
            String endDate = activityCache.stream().map(x -> x.time).max(LocalDateTime::compareTo).get().toString();
            String description = "";
            float ratingX = 0;
            float ratingY = 0;

            Activity activity = new Activity(startDate, endDate, description, ratingX, ratingY);

            LocationAndTime locationAndTime = activityCache.stream().findFirst().get();
            Place place = new Place(locationAndTime.address, locationAndTime.longitude, locationAndTime.latitude);

            ActivityAndPlace activityAndPlace = new ActivityAndPlace(activity, place);

            activityCache = new ArrayList<>();
            return Optional.of(activityAndPlace);
        } else if (activityCache.size() == 1) {
            activityCache.remove(0);
            activityCache.add(new LocationAndTime(address, location, location.getLongitude(), location.getLatitude(), time));
            return Optional.empty();
        }

        return Optional.empty();
    }

    private static boolean IsSameActivity(String address, Location newLocation) {
        return activityCache.stream().anyMatch(x -> {
            Location oldLocation = new Location(x.location);
            float distance = oldLocation.distanceTo(newLocation);

            return x.address.equals(address) || distance < 20;
        });
    }

}
