package de.cmlab.drm_app2.backgroundServices.reminder;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.time.LocalTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

import de.cmlab.drm_app2.backgroundServices.NotificationCreator;
import de.cmlab.drm_app2.infrastructure.entity.NotificationReminderSetting;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;

public class ReminderService extends Service {

    private final int FIFTEEN_MINUTE = 900000;
    private final int ONE_SECOND = 1000;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(NotificationCreator.getNotificationId(),
                NotificationCreator.getNotification(this));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Starting reminderService");

        new Thread(() -> {
            ActivityViewModel activityViewModel = new ActivityViewModel(getApplication());
            ZoneId zoneId = ZoneId.of("Europe/Berlin");

            while (true) {
                try {
                    NotificationReminderSetting notificationReminderSetting = activityViewModel.getReminderSetting();
                    if (notificationReminderSetting != null) {
                        String reminderTime = notificationReminderSetting.getTime();
                        int hour = Integer.parseInt(reminderTime.substring(0, 2));
                        int minute = Integer.parseInt(reminderTime.substring(3, 5));

                        LocalTime now = LocalTime.now(zoneId);
                        int diffHour = now.getHour() > hour ? now.getHour() - hour : hour - now.getHour();

                        if (diffHour > 1 || now.getHour() > hour) {
                            Thread.sleep(FIFTEEN_MINUTE);
                        } else {
                            int diffMin = now.getMinute() > minute ? now.getMinute() - minute : minute - now.getMinute();
                            if (diffMin == 0) {
                                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                NotificationCreator.updateNotificationWithCurrentMessage(notificationManager, this, "Please Tap to add your feeling about today activities");
                                Thread.sleep(FIFTEEN_MINUTE);
                            } else {
                                Thread.sleep(ONE_SECOND);
                            }
                        }
                    }
                } catch (Exception ignored) {}
            }
        }, "ReminderService-Thread ").start();
        return START_STICKY;
    }
}
