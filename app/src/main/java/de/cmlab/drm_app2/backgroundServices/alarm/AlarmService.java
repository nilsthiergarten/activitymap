package de.cmlab.drm_app2.backgroundServices.alarm;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

import de.cmlab.drm_app2.backgroundServices.NotificationCreator;
import de.cmlab.drm_app2.infrastructure.entity.Alarm;
import de.cmlab.drm_app2.infrastructure.repository.AlarmRepository;
import de.cmlab.drm_app2.infrastructure.repository.DailyActivityDateRepository;

public class AlarmService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(NotificationCreator.getNotificationId(),
                NotificationCreator.getNotification(this));

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Starting alarmService");

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        saveMorningAlarm(alarmManager);

        return START_STICKY;
    }

    @SuppressLint("MissingPermission")
    private void saveMorningAlarm(AlarmManager alarmManager) {
        new Thread(() -> {
            AlarmRepository alarmRepository = new AlarmRepository(getApplication());
            DailyActivityDateRepository dailyActivityDateRepository = new DailyActivityDateRepository(getApplication());

            while (true) {
                int currentHour = LocalDateTime.now().getHour();
                int diffUntilMidnightInHours = 24 - currentHour;
                long waitingTimeUntilMidnightInHours = TimeUnit.HOURS.toMillis(diffUntilMidnightInHours);

                if ((currentHour >= 10 && currentHour <= 23)) {
                    try {
                        Log.e("NUMBER OF HOUR (S) BEFORE NEXT CHECK ", String.valueOf(diffUntilMidnightInHours));

                        Thread.sleep(waitingTimeUntilMidnightInHours);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {

                    AlarmManager.AlarmClockInfo alarmClockInfo = alarmManager.getNextAlarmClock();
                    if (alarmClockInfo != null) {
                        long nextAlarm = alarmManager.getNextAlarmClock().getTriggerTime();
                        LocalDateTime alarmDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(nextAlarm), ZoneId.systemDefault());
                        String morningAlarm = alarmDateTime.getHour() + ":" + alarmDateTime.getMinute();

                        long todayActivityDateId = dailyActivityDateRepository.getTodayActivityDateId();
                        Alarm alarm = new Alarm(todayActivityDateId, morningAlarm);
                        alarmRepository.insert(alarm);

                        Log.e("----- NEXT ALARM IS AT ----- ", morningAlarm);

                        try {
                            Thread.sleep(waitingTimeUntilMidnightInHours);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, "AlarmService-Thread ").start();
    }
}
