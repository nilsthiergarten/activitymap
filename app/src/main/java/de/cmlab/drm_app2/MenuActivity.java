package de.cmlab.drm_app2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.Image;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;

import de.cmlab.drm_app2.infrastructure.entity.Activity;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.Place;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorageImpl;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.Keys;
import de.cmlab.drm_app2.infrastructure.utils.MapController;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;
import de.cmlab.drm_app2.ui.RatingView;
import lombok.SneakyThrows;

import static de.cmlab.drm_app2.infrastructure.utils.MapController.initSymbolLayer;

public class MenuActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ActivityViewModel activityViewModel = new ActivityViewModel(getApplication());
    private SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM \n HH:mm");

    private Optional<ActivityAndPlace> activityAndPlaceToEdit;

    private MapView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Intent intent = getIntent();
        ActivityAndPlace passedActivityAndPlaceToEdit = (ActivityAndPlace) intent.getSerializableExtra(ProfileDetailActivity.detailInfo);

        if (passedActivityAndPlaceToEdit == null) {
            activityAndPlaceToEdit = Optional.empty();
        } else {
            activityAndPlaceToEdit = Optional.of(passedActivityAndPlaceToEdit);
            initAllDataForEditing();
        }

        mapView = findViewById(R.id.activity_menu_mapView);
        mapView.getMapAsync(this);
    }

    private void initAllDataForEditing() {
        ActivityAndPlace activityAndPlace = activityAndPlaceToEdit.get();
        Activity activity = activityAndPlace.getActivity();

        List<Button> buttonsToDisable = new ArrayList<>();
        buttonsToDisable.add(findViewById(R.id.activity_menu_location_button));

        LocalDateTime startTime = LocalDateTime.parse(activity.getStartTime());
        TextView startTimeTextView = findViewById(R.id.activity_menu_start_time);
        startTimeTextView.setVisibility(View.VISIBLE);
        startTimeTextView.setText(getFormattedString(startTime.getHour(), startTime.getMinute()));
        startTimeTextView.setOnClickListener(this::setStartTime);
        buttonsToDisable.add(findViewById(R.id.activity_menu_start_time_button));

        LocalDateTime endTime = LocalDateTime.parse(activity.getEndTime());
        TextView endTimeTextView = findViewById(R.id.activity_menu_end_time);
        endTimeTextView.setVisibility(View.VISIBLE);
        endTimeTextView.setText(getFormattedString(endTime.getHour(), endTime.getMinute()));
        endTimeTextView.setOnClickListener(this::setEndTime);
        buttonsToDisable.add(findViewById(R.id.activity_menu_end_time_button));

        if (activity.getDescription() != null && !activity.getDescription().isEmpty()) {
            TextView descriptionTextView = findViewById(R.id.activity_menu_description);
            descriptionTextView.setText(activity.getDescription());
            descriptionTextView.setOnClickListener(this::setDescription);
            buttonsToDisable.add(findViewById(R.id.activity_menu_description_button));
        }

        mapView = findViewById(R.id.activity_menu_mapView);
        mapView.setVisibility(View.VISIBLE);

        RatingView ratingView = findViewById(R.id.activity_menu_ratingView);
        ratingView.setVisibility(View.VISIBLE);
        buttonsToDisable.add(findViewById(R.id.activity_menu_rating_button));

        buttonsToDisable.forEach(b -> b.setVisibility(View.GONE));
        checkIfAllButtonsSetAndAllowSave();
    }

    public void setLocation(View view) {
        Button setLocationButton = findViewById(R.id.activity_menu_location_button);
        setLocationButton.setVisibility(View.GONE);

        mapView = findViewById(R.id.activity_menu_mapView);
        mapView.setVisibility(View.VISIBLE);

        checkIfAllButtonsSetAndAllowSave();
    }

    @SneakyThrows
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        LatLng latLng;

        if (activityAndPlaceToEdit.isPresent()) {
            Place place = activityAndPlaceToEdit.get().getPlace();
            latLng = new LatLng(place.getLatitude(), place.getLongitude());
        } else {
            Location currentLocation = (Location) InMemoryStorageImpl.getInstance().retrieve(Keys.Location);

            try {
                latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            } catch (Exception ignored) {
                try {
                    Place place = Executors.newSingleThreadExecutor().submit(
                            () -> activityViewModel.getActivitiesWithPlace().get(0).getPlace()).get();
                    latLng = new LatLng(place.getLatitude(), place.getLongitude());
                } catch (Exception ignored2) {
                    // If everything fails, default to germany
                    // Taken from https://worldpopulationreview.com/country-locations/where-is-germany
                    latLng = new LatLng(51.1657, 10.4515);
                }
            }
        }

        ImageButton currentLocationButton = findViewById(R.id.currentLocationButton);
        currentLocationButton.setVisibility(View.VISIBLE);

        currentLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapboxMap.getStyle(new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(Style style) {
                        MapController.resetLocation(MenuActivity.this, mapboxMap, style);
                    }
                });
            }
        });

        ImageView searchIcon =  findViewById(R.id.searchIcon);
        searchIcon.setVisibility(View.VISIBLE);
        searchIcon.setY(searchIcon.getY()+8);

        mapboxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(Style style) {
                MapController.enableLocationComponent(MenuActivity.this, mapboxMap, style);
            }
        });



        mapboxMap.setStyle(initSymbolLayer());
        MapController.setCameraPositionForCurrentActivity(mapboxMap, latLng);
    }

    public void setStartTime(View view) {
        TimePickerDialog.OnTimeSetListener timePickerListener = (view1, hour, minute) -> {
            Button startTimeButton = findViewById(R.id.activity_menu_start_time_button);
            startTimeButton.setVisibility(View.GONE);

            TextView startTimeTextView = findViewById(R.id.activity_menu_start_time);
            startTimeTextView.setVisibility(View.VISIBLE);
            startTimeTextView.setText(getFormattedString(hour, minute));
            startTimeTextView.setOnClickListener(this::setStartTime);

            if (!checkIfEndTimeIsAfterStartTime()) {
                Toast.makeText(this, "End time is before start time", Toast.LENGTH_SHORT).show();
            }
            checkIfAllButtonsSetAndAllowSave();
        };

        createTimePickDialog(timePickerListener, true);
    }

    public void setEndTime(View view) {
        TimePickerDialog.OnTimeSetListener timePickerListener = (view1, hour, minute) -> {
            Button endTimeButton = findViewById(R.id.activity_menu_end_time_button);
            endTimeButton.setVisibility(View.GONE);

            TextView endTimeTextView = findViewById(R.id.activity_menu_end_time);
            endTimeTextView.setVisibility(View.VISIBLE);
            endTimeTextView.setText(getFormattedString(hour, minute));
            endTimeTextView.setOnClickListener(this::setEndTime);

            if (!checkIfEndTimeIsAfterStartTime()) {
                Toast.makeText(this, "End time is before start time", Toast.LENGTH_SHORT).show();
            }
            checkIfAllButtonsSetAndAllowSave();
        };

        createTimePickDialog(timePickerListener, false);
    }

    public void setDescription(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Description");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // dirty hack: Html.fromHtml is used to color AlertDialog text. See https://stackoverflow.com/a/33439849
        builder.setNegativeButton(Html.fromHtml("<font color='#84A0EF'>Cancel</font>"), (dialog, which) -> dialog.cancel());
        builder.setPositiveButton(Html.fromHtml("<font color='#84A0EF'>Yes</font>"), (dialog, which) -> {
            String descriptionText = input.getText().toString();

            Button descriptionButton = findViewById(R.id.activity_menu_description_button);
            descriptionButton.setVisibility(View.GONE);

            TextView descriptionTextView = findViewById(R.id.activity_menu_description);
            descriptionTextView.setText(descriptionText);
            descriptionTextView.setOnClickListener(this::setDescription);

            checkIfAllButtonsSetAndAllowSave();
        });

        builder.show();
    }

    public void setRating(View view) {
        Button ratingButton = findViewById(R.id.activity_menu_rating_button);
        ratingButton.setVisibility(View.GONE);

        RatingView ratingView = findViewById(R.id.activity_menu_ratingView);
        ratingView.setVisibility(View.VISIBLE);

        checkIfAllButtonsSetAndAllowSave();
    }

    public void saveActivity(View view) {
        Button saveButton = findViewById(R.id.activity_menu_activity_save_button);

        if (!checkIfEndTimeIsAfterStartTime()) {
            Toast.makeText(this, "End time is before start time", Toast.LENGTH_SHORT).show();
            return;
        }

        if (buttonIsGreyedOut(saveButton)) {
            Toast.makeText(this, "Please fill out all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        mapView.getMapAsync(mapboxMap -> {
            CameraPosition cameraPosition = mapboxMap.getCameraPosition();

            LatLng cameraCoordinates = cameraPosition.target;
            Geocoder geocoder = new Geocoder(this);

            Activity activity = null;
            Place place;

            try {
                activity = getActivityFromUI();
                Address address = geocoder.getFromLocation(cameraCoordinates.getLatitude(), cameraCoordinates.getLongitude(), 1).get(0);
                place = new Place(address.getAddressLine(0), cameraCoordinates.getLongitude(), cameraCoordinates.getLatitude());
            } catch (Exception ignored) {
                place = new Place("", cameraCoordinates.getLongitude(), cameraCoordinates.getLatitude());
            }

            ActivityAndPlace activityAndPlace = new ActivityAndPlace(activity, place);

            new Thread(() -> {
                if (activityAndPlaceToEdit.isPresent()) {
                    ActivityAndPlace activityAndPlaceToUpdate = activityAndPlace;
                    activityAndPlaceToUpdate.getActivity().setActivityId(activityAndPlaceToEdit.get().getActivity().getActivityId());
                    activityAndPlaceToUpdate.getActivity().setDailyActivityDateId(activityAndPlaceToEdit.get().getActivity().getDailyActivityDateId());
                    activityViewModel.updateActivityAndPlace(activityAndPlaceToUpdate);
                } else {
                    activityViewModel.insertActivityWithPlace(activityAndPlace);
                }
                runOnUiThread(() -> Toast.makeText(this, "Event stored to database", Toast.LENGTH_SHORT).show());

                Intent backToMain = new Intent(this, MainActivity.class);
                startActivity(backToMain);
            }).start();
        });
    }

    private void createTimePickDialog(TimePickerDialog.OnTimeSetListener eventHandler, boolean isStartTime) {
        TimePickerDialog timePickerDialog;

        if (activityAndPlaceToEdit.isPresent() && isStartTime) {
            LocalDateTime startTime = LocalDateTime.parse(activityAndPlaceToEdit.get().getActivity().getStartTime());
            timePickerDialog = new TimePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, eventHandler, startTime.getHour(), startTime.getMinute(), true);
        } else if (activityAndPlaceToEdit.isPresent() && !isStartTime) {
            LocalDateTime endTime = LocalDateTime.parse(activityAndPlaceToEdit.get().getActivity().getEndTime());
            timePickerDialog = new TimePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, eventHandler, endTime.getHour(), endTime.getMinute(), true);
        } else {
            timePickerDialog = new TimePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, eventHandler, 12, 0, true);
        }

        timePickerDialog.show();

        int color = getResources().getColor(R.color.blue_500);
        timePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(color);
        timePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(color);
    }

    private String getFormattedString(int hour, int minute) {
        LocalDate selectedDate = (LocalDate) InMemoryStorageImpl.getInstance().retrieve(Keys.SelectedDate);

        Date date = new Date();
        date.setMonth(selectedDate.getMonthValue() - 1);
        date.setDate(selectedDate.getDayOfMonth());
        date.setHours(hour);
        date.setMinutes(minute);

        return formatter.format(date);
    }

    private String formatStringForDb(String unformattedString) throws ParseException {
        Date date = formatter.parse(unformattedString);

        LocalDate selectedDate = (LocalDate) InMemoryStorageImpl.getInstance().retrieve(Keys.SelectedDate);

        // ugly hack: There has to be a better way to create this
        return String.format("%04d-%02d-%02dT%02d:%02d:%02d.000",
                selectedDate.getYear(),
                selectedDate.getMonthValue(),
                selectedDate.getDayOfMonth(),
                date.getHours(),
                date.getMinutes(),
                date.getSeconds());
    }

    private void checkIfAllButtonsSetAndAllowSave() {
        List<Button> buttonList = new ArrayList<>();

        buttonList.add(findViewById(R.id.activity_menu_location_button));
        buttonList.add(findViewById(R.id.activity_menu_start_time_button));
        buttonList.add(findViewById(R.id.activity_menu_end_time_button));
        buttonList.add(findViewById(R.id.activity_menu_description_button));
        buttonList.add(findViewById(R.id.activity_menu_rating_button));

        if (buttonList.stream().allMatch(x -> x.getVisibility() == View.GONE) && checkIfEndTimeIsAfterStartTime()) {
            Button saveButton = findViewById(R.id.activity_menu_activity_save_button);
            saveButton.setAlpha(1);
        }
    }

    private boolean checkIfEndTimeIsAfterStartTime() {
        String startTimeText = ((TextView) findViewById(R.id.activity_menu_start_time)).getText().toString();
        String endTimeText = ((TextView) findViewById(R.id.activity_menu_end_time)).getText().toString();

        if (startTimeText.length() > 0 && endTimeText.length() > 0) {
            try {
                Date startTime = formatter.parse(startTimeText);
                Date endTime = formatter.parse(endTimeText);

                return startTime.compareTo(endTime) <= 0;
            } catch (ParseException ignored) { }
        }

        return true;
    }

    private boolean buttonIsGreyedOut(Button saveButton) {
        return saveButton.getAlpha() == 0.5;
    }

    private Activity getActivityFromUI() throws ParseException {
        String startTimeWithoutFormat = ((TextView) findViewById(R.id.activity_menu_start_time)).getText().toString();
        String endTimeWithoutFormat = ((TextView) findViewById(R.id.activity_menu_end_time)).getText().toString();

        String startTime = formatStringForDb(startTimeWithoutFormat);
        String endTime = formatStringForDb(endTimeWithoutFormat);
        String description = ((TextView) findViewById(R.id.activity_menu_description)).getText().toString();

        RatingView ratingView = findViewById(R.id.activity_menu_ratingView);

        float ratingX = ratingView.getRatingX();
        float ratingY = ratingView.getRatingY();

        return new Activity(startTime, endTime, description, ratingX, ratingY);
    }
}