package de.cmlab.drm_app2.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import de.cmlab.drm_app2.R;

import static java.lang.String.valueOf;

public class RatingView extends View {

    private final int paintColor = Color.BLACK;
    private Paint drawPaint;

    public List<Point> points;
    public Point point;


    public void setRatingX(float ratingX) {
        this.ratingX = ratingX;
    }

    public void setRatingY(float ratingY) {
        this.ratingY = ratingY;
    }

    public float ratingX;
    public float ratingY;
    //Pair<Double, Double> rating = new Pair<>(ratingX,ratingY);

    private int i;

    Bitmap dot = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.small_dot);


    public RatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);


        setupPaint();
        points = new ArrayList<Point>();
        point = new Point();

    }


    @Override
    protected void onDraw(Canvas canvas) {

        if (i<2){
            canvas.drawBitmap(dot,canvas.getWidth()/2-dot.getWidth()/2,canvas.getHeight()/2-dot.getHeight()/2,drawPaint);
            i++;
        }
        if (i>=2)
        canvas.drawBitmap(dot,point.x-dot.getWidth()/2,point.y-dot.getHeight()/2,drawPaint);

        this.ratingX = (float) point.x/canvas.getWidth();
        this.ratingY = (float) point.y/canvas.getHeight();
        Log.d("Rating: ", ratingX + ", " + ratingY);

    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        i++;
        float touchX = event.getX();
        float touchY = event.getY();
        Log.d("Coordinates", touchX + "," + touchY);
        point.set(Math.round(touchX),Math.round(touchY));
        points.add(new Point(Math.round(touchX), Math.round(touchY)));
        postInvalidate();
        return true;
    }


    private void setupPaint() {
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(5);
        drawPaint.setStyle(Paint.Style.FILL);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

    }

    public float getRatingX() {
        return ratingX;
    }

    public float getRatingY() {
        return ratingY;
    }

}
