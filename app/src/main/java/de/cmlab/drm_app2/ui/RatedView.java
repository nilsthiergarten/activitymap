package de.cmlab.drm_app2.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;



import de.cmlab.drm_app2.R;

public class RatedView extends View {

    private final int paintColor = Color.BLACK;
    private Paint drawPaint;

    public List<Point> points;
    public Point point;

    //public PointF rating;

    public void setRatingX(float ratingX) {
        this.ratingX = ratingX;
    }

    public void setRatingY(float ratingY) {
        this.ratingY = ratingY;
    }

    public float ratingX;
    public float ratingY;

    private int i;

    Bitmap dot = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.small_dot);

    public RatedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);

        Log.d("In on draw in rated view: ", String.valueOf(ratingX));
        setupPaint();
        points = new ArrayList<Point>();
        point = new Point();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d("In on draw: ", String.valueOf(ratingX));
        canvas.drawBitmap(dot, (float) this.ratingX*canvas.getWidth()-dot.getWidth()/2, (float) this.ratingY*canvas.getWidth()-dot.getHeight()/2, drawPaint);

    }

    private void setupPaint() {
        drawPaint = new Paint();

        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(5);
        drawPaint.setStyle(Paint.Style.FILL);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

    }
}
