package de.cmlab.drm_app2.sensors.gps;

import android.location.Address;
import android.location.Location;

import java.io.IOException;

import de.cmlab.drm_app2.sensors.gps.exceptions.NoGPSPermissionException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownAddressesException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownLastLocationException;

public interface PositionService {

    void startGPSTracking() throws NoGPSPermissionException;

    Address getCurrentAddressUsingGPS()
            throws UnknownLastLocationException, UnknownAddressesException, IOException;

    String getCurrentAddressUsingGPSAsString()
            throws UnknownLastLocationException, UnknownAddressesException, IOException;

    Location getCurrentLocationUsingGPS()
            throws NoGPSPermissionException;

    Address getAddressUsingGPS(Location location)
            throws UnknownLastLocationException, UnknownAddressesException, IOException;

    String getAddressUsingGPSAsString(Address address);

    String getAddressUsingLocationAsString(Location location) throws UnknownAddressesException, IOException, UnknownLastLocationException;
}
