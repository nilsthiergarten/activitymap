package de.cmlab.drm_app2.sensors.gps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.util.List;

import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorage;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.Keys;
import de.cmlab.drm_app2.sensors.gps.exceptions.NoGPSPermissionException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownAddressesException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownLastLocationException;

public class PositionServiceImpl implements PositionService {

    private final Context context;
    private final Geocoder geocoder;
    private final InMemoryStorage inMemoryStorage;

    private final int GPS_UPDATE_TIMER = 5000;
    private final int GPS_UPDATE_RADIUS = 10;

    public PositionServiceImpl(Context context, Geocoder geocoder, InMemoryStorage inMemoryStorage) {
        this.context = context;
        this.geocoder = geocoder;
        this.inMemoryStorage = inMemoryStorage;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void startGPSTracking() throws NoGPSPermissionException {
        if (!this.isGPSPermissionEnabled(context)) {
            throw new NoGPSPermissionException();
        }

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new GPSTracker(inMemoryStorage);
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, GPS_UPDATE_TIMER, GPS_UPDATE_RADIUS, locationListener);
    }

    @Override
    public Location getCurrentLocationUsingGPS() {
        return (Location) inMemoryStorage.retrieve(Keys.Location);
    }

    @Override
    public Address getCurrentAddressUsingGPS()
            throws UnknownLastLocationException, UnknownAddressesException, IOException {
        Location lastKnownLocation = getCurrentLocationUsingGPS();

        return getAddressUsingGPS(lastKnownLocation);
    }

    @Override
    public String getCurrentAddressUsingGPSAsString() throws UnknownLastLocationException, UnknownAddressesException, IOException {
        return getAddressUsingGPSAsString(getCurrentAddressUsingGPS());
    }

    @Override
    public Address getAddressUsingGPS(Location location) throws UnknownLastLocationException, UnknownAddressesException, IOException {
        if (location == null) {
            throw new UnknownLastLocationException();
        }

        List<Address> addresses = geocoder.getFromLocation(
                location.getLatitude(),
                location.getLongitude(),
                1);

        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0);
        } else {
            throw new UnknownAddressesException();
        }
    }

    @Override
    public String getAddressUsingGPSAsString(Address address) {
        return address.getAddressLine(0);
    }

    @Override
    public String getAddressUsingLocationAsString(Location location) throws UnknownAddressesException, IOException, UnknownLastLocationException {
        return getAddressUsingGPSAsString(getAddressUsingGPS(location));
    }

    private boolean isGPSPermissionEnabled(Context context) {
        return ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

}
