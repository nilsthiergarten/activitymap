package de.cmlab.drm_app2.sensors.gps;

import android.location.Location;
import android.location.LocationListener;

import androidx.annotation.NonNull;

import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorage;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.Keys;

public class GPSTracker implements LocationListener {

    private final InMemoryStorage memoryStore;

    public GPSTracker(InMemoryStorage memoryStore) {
        this.memoryStore = memoryStore;
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        this.memoryStore.store(Keys.Location, location);
    }
}
