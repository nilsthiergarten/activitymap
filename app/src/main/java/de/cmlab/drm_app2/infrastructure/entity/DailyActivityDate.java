package de.cmlab.drm_app2.infrastructure.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

import lombok.Data;

@Data
@Entity
public class DailyActivityDate {

    @PrimaryKey(autoGenerate = true)
    private long dailyActivityDateId;

    private Date date;

    public DailyActivityDate(Date date) {
        this.date = date;
    }

    public long getDailyActivityDateId() {
        return dailyActivityDateId;
    }

    public void setDailyActivityDateId(long dailyActivityDateId) {
        this.dailyActivityDateId = dailyActivityDateId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
