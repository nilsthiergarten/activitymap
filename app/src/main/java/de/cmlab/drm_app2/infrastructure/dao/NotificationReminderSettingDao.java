package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import de.cmlab.drm_app2.infrastructure.entity.NotificationReminderSetting;

@Dao
public interface NotificationReminderSettingDao {

    @Insert
    void insert(NotificationReminderSetting notification);

    @Update
    void update(NotificationReminderSetting notification);

    @Query("SELECT * FROM NotificationReminderSetting")
    NotificationReminderSetting getSetting();

}
