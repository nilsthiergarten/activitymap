package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;

@Dao
public interface ActivityAndPlaceDao {

    @Transaction
    @Query("Select * from place")
    public List<ActivityAndPlace> getActivitiesAndPlace();
}
