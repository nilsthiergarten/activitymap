package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.Place;

@Dao
public interface PlaceDao {

    @Insert
    void insert(Place place);

    @Query("SELECT * FROM Place")
    List<Place> getAll();

    @Query("SELECT * FROM Place WHERE address = :address")
    Place getPlaceForAddress(String address);
}
