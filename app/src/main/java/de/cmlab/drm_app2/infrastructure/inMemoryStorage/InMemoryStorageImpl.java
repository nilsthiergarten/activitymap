package de.cmlab.drm_app2.infrastructure.inMemoryStorage;

import java.util.concurrent.ConcurrentHashMap;

public class InMemoryStorageImpl implements InMemoryStorage {

    private static InMemoryStorageImpl instance;

    private ConcurrentHashMap<Object, Object> store = new ConcurrentHashMap<>();

    private InMemoryStorageImpl() {}

    public static InMemoryStorageImpl getInstance () {
        if (InMemoryStorageImpl.instance == null) {
            InMemoryStorageImpl.instance = new InMemoryStorageImpl();
        }

        return InMemoryStorageImpl.instance;
    }

    @Override
    public void store(Keys key, Object value) {
        store.put(key, value);
    }

    @Override
    public Object retrieve(Keys key) {
        return store.get(key);
    }

    @Override
    public void delete(Keys key) {
        store.remove(key);
    }
}
