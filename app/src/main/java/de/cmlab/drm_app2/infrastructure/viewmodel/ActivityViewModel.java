package de.cmlab.drm_app2.infrastructure.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import de.cmlab.drm_app2.infrastructure.entity.Activity;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.DailyActivityDate;
import de.cmlab.drm_app2.infrastructure.entity.NotificationReminderSetting;
import de.cmlab.drm_app2.infrastructure.entity.Place;
import de.cmlab.drm_app2.infrastructure.repository.ActivityRepository;
import de.cmlab.drm_app2.infrastructure.repository.AlarmRepository;
import de.cmlab.drm_app2.infrastructure.repository.DailyActivityAndActivitiesAndAlarmRepository;
import de.cmlab.drm_app2.infrastructure.repository.DailyActivityDateRepository;
import de.cmlab.drm_app2.infrastructure.repository.NotificationReminderSettingRepository;
import de.cmlab.drm_app2.infrastructure.repository.PlaceRepository;
import de.cmlab.drm_app2.infrastructure.utils.DateComparator;

public class ActivityViewModel extends AndroidViewModel {

    private static ActivityViewModel instance;

    public ActivityRepository activityRepository;
    public AlarmRepository alarmRepository;
    public PlaceRepository placeRepository;
    public DailyActivityDateRepository dailyActivityDateRepository;
    public DailyActivityAndActivitiesAndAlarmRepository dailyActivityAndActivitiesAndAlarmRepository;
    public NotificationReminderSettingRepository notificationReminderSettingRepository;

    public ActivityViewModel(Application application) {
        super(application);
        this.alarmRepository = new AlarmRepository(application);
        this.activityRepository = new ActivityRepository(application);
        this.placeRepository = new PlaceRepository(application);
        this.dailyActivityDateRepository = new DailyActivityDateRepository(application);
        this.dailyActivityAndActivitiesAndAlarmRepository = new DailyActivityAndActivitiesAndAlarmRepository(application);
        this.notificationReminderSettingRepository = new NotificationReminderSettingRepository(application);
    }

    public static ActivityViewModel getInstance() {
        return ActivityViewModel.instance;
    }

    public static void setInstance(ActivityViewModel activityViewModel) {
        ActivityViewModel.instance = activityViewModel;
    }

    public void insertActivity(Activity activity) {
        activityRepository.insert(activity);
    }

    public void insertReminderSetting(NotificationReminderSetting notificationReminderSetting){
        this.notificationReminderSettingRepository.insertReminderSetting(notificationReminderSetting);
    }

    public NotificationReminderSetting getReminderSetting(){
       return this.notificationReminderSettingRepository.getSetting();
    }

    public void updateNotificationSetting(NotificationReminderSetting notificationReminderSetting){
        this.notificationReminderSettingRepository.updateSetting(notificationReminderSetting);
    }

    public List<ActivityAndPlace> getActivitiesWithPlace() {
        List<Place> allPlaces = placeRepository.getAll();
        List<Activity> allActivities = activityRepository.getActivities();

        List<ActivityAndPlace> activityAndPlaces = new ArrayList<>();

        allActivities.forEach(x -> {
            Optional<Place> currentPlace = allPlaces.stream().filter(p -> p.getPlaceId() == x.getActivityPlaceId()).findFirst();

            if (currentPlace.isPresent()) {
                ActivityAndPlace activityAndPlace = new ActivityAndPlace(x, currentPlace.get());
                activityAndPlaces.add(activityAndPlace);
            }
        });

        return activityAndPlaces;
    }

    public List<ActivityAndPlace> getActivitiesWithPlaceForLocalDate(LocalDate localDate) {
        List<Place> allPlaces = placeRepository.getAll();
        List<Activity> allActivities = activityRepository.getForLocalDate(localDate);

        List<ActivityAndPlace> activityAndPlaces = new ArrayList<>();

        allActivities.forEach(x -> {
            Optional<Place> currentPlace = allPlaces.stream().filter(p -> p.getPlaceId() == x.getActivityPlaceId()).findFirst();

            if (currentPlace.isPresent()) {
                ActivityAndPlace activityAndPlace = new ActivityAndPlace(x, currentPlace.get());
                activityAndPlaces.add(activityAndPlace);
            }
        });

        return activityAndPlaces;
    }

    // Needs to be thread-safe
    // Reason:
    // We create entries in the DB and fetch the latest entry (the inserted entries) then immediately for their ID
    // If we are not thread-safe here, we can mix up the IDs from these inserts and fetches
    public synchronized void insertActivityWithPlace(ActivityAndPlace activityAndPlace) {
        DailyActivityDate lastDate = dailyActivityDateRepository.getLastDate();

        if (lastDate == null || !DateComparator.isSameAsToday(lastDate.getDate())) {
            // create new dailyActivity for today
            lastDate = new DailyActivityDate(new Date());
            dailyActivityDateRepository.insert(lastDate);
            lastDate = dailyActivityDateRepository.getLastDate();
        }

        storeActivityAndPlaceForDate(activityAndPlace, lastDate);
    }

    public synchronized void updateActivityAndPlace(ActivityAndPlace activityAndPlace) {
        Place placeFromDb = getPlace(activityAndPlace.getPlace());

        activityAndPlace.getActivity().setActivityPlaceId(placeFromDb.getPlaceId());
        activityAndPlace.getPlace().setPlaceId(placeFromDb.getPlaceId());

        activityRepository.update(activityAndPlace.getActivity());
    }

    private void storeActivityAndPlaceForDate(ActivityAndPlace activityAndPlace, DailyActivityDate currentDate) {
        Place placeFromDb = getPlace(activityAndPlace.getPlace());

        activityAndPlace.getActivity().setDailyActivityDateId(currentDate.getDailyActivityDateId());
        activityAndPlace.getActivity().setActivityPlaceId(placeFromDb.getPlaceId());

        activityAndPlace.getPlace().setPlaceId(placeFromDb.getPlaceId());

        activityRepository.insert(activityAndPlace.getActivity());
    }

    public void updateActivity(Activity activity) {
        activityRepository.update(activity);
    }

    private Place getPlace(Place place) {
        Place placeFromDb = placeRepository.getPlaceForAddress(place.getAddress());

        if (placeFromDb == null) {
            placeRepository.insert(place);
            placeFromDb = placeRepository.getPlaceForAddress(place.getAddress());
        }
        return placeFromDb;
    }
}
