package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import java.util.Date;
import java.util.List;

import de.cmlab.drm_app2.infrastructure.dao.DailyActivityDateDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.DailyActivityDate;
import de.cmlab.drm_app2.infrastructure.utils.DateComparator;

public class DailyActivityDateRepository {
    private DailyActivityDateDao dailyActivityDateDao;

    public DailyActivityDateRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        dailyActivityDateDao = db.dailyActivityDao();
    }

    public void insert(DailyActivityDate dailyActivityDate) {
        this.dailyActivityDateDao.insert(dailyActivityDate);
    }

    public List<DailyActivityDate> getAll() {
        return dailyActivityDateDao.getAll();
    }

    public DailyActivityDate getLastDate() {
        return dailyActivityDateDao.getLastDate();
    }

    public DailyActivityDate getDateById(long id) {
        return dailyActivityDateDao.getById(id);
    }

    /**
     * Check if the today date was already saved otherwise create the date and save
     *
     * @return the id of the today date
     */
    public synchronized long getTodayActivityDateId() {
        DailyActivityDate lastActivityDate = this.getLastDate();

        if ((lastActivityDate != null) && DateComparator.isSameAsToday(lastActivityDate.getDate())) {
            return lastActivityDate.getDailyActivityDateId();
        }

        this.insert(new DailyActivityDate(new Date()));
        return dailyActivityDateDao.getLastDate().getDailyActivityDateId();
    }
}
