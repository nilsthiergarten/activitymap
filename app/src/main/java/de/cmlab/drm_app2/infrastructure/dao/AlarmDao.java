package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.Alarm;

@Dao
public interface AlarmDao {

    @Insert
    void insert(Alarm alarm);

    @Update
    void update(Alarm alarm);

    @Query("SELECT * FROM Alarm")
    List<Alarm> getAll();

    @Query("SELECT * FROM Alarm WHERE alarmId = :id")
    Alarm getById(Long id);

    @Query("SELECT * FROM Alarm ORDER BY alarmId DESC LIMIT 1")
    Alarm getLastAlarm();
}
