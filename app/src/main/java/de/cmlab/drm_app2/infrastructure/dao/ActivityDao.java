package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.Activity;

@Dao
public interface ActivityDao {

    @Insert
    void insert(Activity... activity);

    @Update
    void update(Activity... activity);

    @Query("SELECT * FROM activity_table")
    List<Activity> getAll();

    @Query("SELECT * FROM activity_table WHERE activityId = :id")
    Activity getById(Long id);

    @Query("DELETE FROM activity_table")
    void deleteAll();

    @Query("DELETE FROM activity_table WHERE activityId = :id")
    void deleteById(Long id);

    @Query("SELECT * FROM activity_table ORDER BY activityId DESC LIMIT 1")
    Activity getLatest();

    @Query("SELECT * FROM activity_table WHERE start_time LIKE :datePrefix")
    List<Activity> getForLocalDate(String datePrefix);
}
