package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.dao.ActivityAndPlaceDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;

public class ActivityAndPlaceRepository {

    private ActivityAndPlaceDao activityAndPlaceDao;

    public ActivityAndPlaceRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        this.activityAndPlaceDao = db.activityAndPlaceAndAlarmDao();
    }

    public List<ActivityAndPlace> getActivityAndPlaceDao() {
        return activityAndPlaceDao.getActivitiesAndPlace();
    }
}
