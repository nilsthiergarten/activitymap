package de.cmlab.drm_app2.infrastructure.entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class DailyActivityAndActivitiesAndAlarm {
    @Embedded
    private DailyActivityDate dailyActivityDate;

    @Relation(parentColumn = "dailyActivityDateId", entityColumn = "dailyActivityDateId")
    private List<Activity> activities;

    @Relation(parentColumn = "dailyActivityDateId", entityColumn = "dailyActivityDateId")
    private Alarm alarm;

    public DailyActivityAndActivitiesAndAlarm(DailyActivityDate dailyActivityDate, List<Activity> activities, Alarm alarm) {
        this.dailyActivityDate = dailyActivityDate;
        this.activities = activities;
        this.alarm = alarm;
    }

    public DailyActivityDate getDailyActivityDate() {
        return dailyActivityDate;
    }

    public void setDailyActivityDate(DailyActivityDate dailyActivityDate) {
        this.dailyActivityDate = dailyActivityDate;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }
}
