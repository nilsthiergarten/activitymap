package de.cmlab.drm_app2.infrastructure.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

import lombok.Data;

@Data
@Entity(tableName = "activity_table")
public class Activity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long activityId;

    private long dailyActivityDateId;

    private long activityPlaceId;

    @ColumnInfo(name = "start_time")
    private String startTime;

    @ColumnInfo(name = "end_time")
    private String endTime;

    private String description;

    private float ratingX;

    private float ratingY;

    @Ignore
    public Activity(String startTime, String endTime, String description, float ratingX, float ratingY) {
        this.dailyActivityDateId = -1;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.ratingX = ratingX;
        this.ratingY = ratingY;
    }

    public Activity(long dailyActivityDateId, String startTime, String endTime, String description, float ratingX, float ratingY) {
        this.dailyActivityDateId = dailyActivityDateId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.ratingX = ratingX;
        this.ratingY = ratingY;
    }

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public long getDailyActivityDateId() {
        return dailyActivityDateId;
    }

    public void setDailyActivityDateId(long dailyActivityDateId) {
        this.dailyActivityDateId = dailyActivityDateId;
    }

    public long getActivityPlaceId() {
        return activityPlaceId;
    }

    public void setActivityPlaceId(long activityPlaceId) {
        this.activityPlaceId = activityPlaceId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRatingX() {
        return ratingX;
    }

    public void setRatingX(float ratingX) {
        this.ratingX = ratingX;
    }

    public float getRatingY() {
        return ratingY;
    }

    public void setRatingY(float ratingY) {
        this.ratingY = ratingY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return activityId == activity.activityId &&
                dailyActivityDateId == activity.dailyActivityDateId &&
                Objects.equals(activity.ratingX, ratingX) &&
                Objects.equals(startTime, activity.startTime) &&
                Objects.equals(endTime, activity.endTime) &&
                Objects.equals(description, activity.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(activityId, dailyActivityDateId, startTime, endTime, description, ratingX, ratingY);
    }
}
