package de.cmlab.drm_app2.infrastructure.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateComparator {

    static DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Check if the given date is today
     *
     * @param date
     * @return true if dates are the same and false otherwise
     */
    public static boolean isSameAsToday(Date date) {
        try {
            Date formattedDate = formatter.parse(formatter.format(date));
            Date now = formatter.parse(formatter.format(new Date()));

            return formattedDate.compareTo(now) == 0;
        }catch (ParseException e) {}

        return false;
    }
}
