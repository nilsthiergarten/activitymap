package de.cmlab.drm_app2.infrastructure.inMemoryStorage;

public interface InMemoryStorage {
    void store(Keys key, Object value);
    Object retrieve(Keys key);
    void delete(Keys key);
}
