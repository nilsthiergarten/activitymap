package de.cmlab.drm_app2.infrastructure.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Data
@Entity
public class NotificationReminderSetting {
    @PrimaryKey(autoGenerate = true)
    private long settingId;

    private String time;

    public NotificationReminderSetting(String time) {
        this.time = time;
    }

    public long getSettingId() {
        return settingId;
    }

    public void setSettingId(long settingId) {
        this.settingId = settingId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
