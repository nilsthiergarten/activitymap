package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import de.cmlab.drm_app2.infrastructure.dao.ActivityDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.Activity;

public class ActivityRepository {

    private ActivityDao activityDao;

    public ActivityRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        activityDao = db.activityDao();
    }

    public List<Activity> getActivities() {
        return activityDao.getAll();
    }

    public void insert(Activity activity) {
        activityDao.insert(activity);
    }

    public void update(Activity activity) {
        activityDao.update(activity);
    }

    public Activity getLatest() {
        return activityDao.getLatest();
    }

    public List<Activity> getForLocalDate(LocalDate localDate) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String datePrefix = localDate.format(dateTimeFormatter) + "%";

        return activityDao.getForLocalDate(datePrefix);
    }

    public void deleteActivity(Activity activity) {
        activityDao.deleteById(activity.getActivityId());
    }
}
