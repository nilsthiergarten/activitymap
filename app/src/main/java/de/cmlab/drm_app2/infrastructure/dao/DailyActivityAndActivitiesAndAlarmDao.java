package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.DailyActivityAndActivitiesAndAlarm;

@Dao
public interface DailyActivityAndActivitiesAndAlarmDao {

    @Transaction
    @Query("Select * FROM DailyActivityDate")
    public List<DailyActivityAndActivitiesAndAlarm> getAll();
}
