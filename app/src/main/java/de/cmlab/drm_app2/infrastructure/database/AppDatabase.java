package de.cmlab.drm_app2.infrastructure.database;
import android.content.Context;

import de.cmlab.drm_app2.infrastructure.dao.NotificationReminderSettingDao;
import de.cmlab.drm_app2.infrastructure.entity.NotificationReminderSetting;
import de.cmlab.drm_app2.infrastructure.utils.TimestampConverter;
import de.cmlab.drm_app2.infrastructure.dao.ActivityAndPlaceDao;
import de.cmlab.drm_app2.infrastructure.dao.AlarmDao;
import de.cmlab.drm_app2.infrastructure.dao.DailyActivityDateDao;
import de.cmlab.drm_app2.infrastructure.dao.PlaceDao;
import de.cmlab.drm_app2.infrastructure.dao.DailyActivityAndActivitiesAndAlarmDao;
import de.cmlab.drm_app2.infrastructure.entity.Activity;
import de.cmlab.drm_app2.infrastructure.dao.ActivityDao;
import de.cmlab.drm_app2.infrastructure.entity.Alarm;
import de.cmlab.drm_app2.infrastructure.entity.DailyActivityDate;
import de.cmlab.drm_app2.infrastructure.entity.Place;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Alarm.class, Activity.class, Place.class, DailyActivityDate.class, NotificationReminderSetting.class}, version = 1)

@TypeConverters({TimestampConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract ActivityDao activityDao();

    public abstract AlarmDao alarmDao();

    public abstract PlaceDao placeDao();

    public abstract DailyActivityDateDao dailyActivityDao();

    public abstract ActivityAndPlaceDao activityAndPlaceAndAlarmDao();

    public abstract DailyActivityAndActivitiesAndAlarmDao dailyActivityAndActivitiesAndAlarmDao();

    public abstract NotificationReminderSettingDao notificationReminderConfigDao();

    private static volatile AppDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static final ExecutorService databaseReadExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "app_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
