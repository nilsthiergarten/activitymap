package de.cmlab.drm_app2.infrastructure.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.DailyActivityDate;

@Dao
public interface DailyActivityDateDao {

    @Insert
    public void insert(DailyActivityDate dailyActivityDate);

    @Query("SELECT * FROM DailyActivityDate")
    public List<DailyActivityDate> getAll();

    @Query("SELECT * FROM DailyActivityDate ORDER BY dailyActivityDateId DESC LIMIT 1")
    DailyActivityDate getLastDate();

    @Query("SELECT * FROM DailyActivityDate WHERE dailyActivityDateId = :id")
    DailyActivityDate getById(Long id);
}
