package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.dao.PlaceDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.Place;

public class PlaceRepository {

    private PlaceDao placeDao;

    public PlaceRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        placeDao = db.placeDao();
    }

    public void insert(Place place) {
        placeDao.insert(place);
    }

    public List<Place> getAll() {
        AppDatabase.databaseReadExecutor.execute(()-> {
            placeDao.getAll();
        });
        return placeDao.getAll();
    }

    public Place getPlaceForAddress(String address) { return placeDao.getPlaceForAddress(address); }
}
