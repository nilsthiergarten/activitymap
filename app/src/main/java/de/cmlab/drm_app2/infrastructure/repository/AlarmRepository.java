package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.dao.AlarmDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.Alarm;
import de.cmlab.drm_app2.infrastructure.entity.DailyActivityDate;
import de.cmlab.drm_app2.infrastructure.utils.DateComparator;

public class AlarmRepository {

    private AlarmDao alarmDao;
    DailyActivityDateRepository dailyActivityDateRepository;

    public AlarmRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        alarmDao = db.alarmDao();
        dailyActivityDateRepository = new DailyActivityDateRepository(application);
    }

    public Alarm getAlarmById(Long id) {
        return alarmDao.getById(id);
    }

    public List<Alarm> getAll() {
        return alarmDao.getAll();
    }

    public void insert(Alarm alarm) {
        if (!this.todayAlarmAlreadyExist())
            alarmDao.insert(alarm);
    }

    private boolean todayAlarmAlreadyExist() {

        Alarm lastAlarm = alarmDao.getLastAlarm();
        if (lastAlarm != null) {
            DailyActivityDate today = dailyActivityDateRepository.getDateById(lastAlarm.getDailyActivityDateId());
            return DateComparator.isSameAsToday(today.getDate());
        }

        return false;
    }
}
