package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import de.cmlab.drm_app2.infrastructure.dao.NotificationReminderSettingDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.NotificationReminderSetting;

public class NotificationReminderSettingRepository {

    public NotificationReminderSettingDao notificationReminderConfigDao;

    public NotificationReminderSettingRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        this.notificationReminderConfigDao = db.notificationReminderConfigDao();
    }

    public NotificationReminderSetting getSetting() {
        return notificationReminderConfigDao.getSetting();
    }

    public void insertReminderSetting(NotificationReminderSetting setting){
        this.notificationReminderConfigDao.insert(setting);
    }

    public void updateSetting(NotificationReminderSetting notificationReminderSetting) {
        this.notificationReminderConfigDao.update(notificationReminderSetting);
    }
}
