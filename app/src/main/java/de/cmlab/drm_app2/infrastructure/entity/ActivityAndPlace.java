package de.cmlab.drm_app2.infrastructure.entity;

import androidx.room.Embedded;
import androidx.room.Relation;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@Data
public class ActivityAndPlace implements Serializable {

    @Embedded
    private Place place;

    @Relation(parentColumn = "placeId", entityColumn = "activityPlaceId")
    private Activity activity;

    public ActivityAndPlace(Activity activity, Place place) {
        this.activity = activity;
        this.place = place;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityAndPlace that = (ActivityAndPlace) o;
        return Objects.equals(activity, that.activity) &&
                Objects.equals(place, that.place);
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(activity, place);
    }
}
