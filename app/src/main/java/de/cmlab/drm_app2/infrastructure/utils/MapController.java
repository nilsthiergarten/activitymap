package de.cmlab.drm_app2.infrastructure.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.OnLocationClickListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.OnSymbolClickListener;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.cmlab.drm_app2.MainActivity;
import de.cmlab.drm_app2.R;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class MapController {
    public static final String SOURCE_ID = "SOURCE_ID";
    public static final String ICON_ID = "ICON_ID";
    public static final String LAYER_ID = "LAYER_ID";

    private static final String[] textFonts = new String[] {"quicksand"};

    public static void drawAddressOnMap(MapView mapView, MapboxMap mapboxMap, String address, LatLng position, Style style) {
        SymbolManager symbolManager = new SymbolManager(mapView, mapboxMap, style);

        symbolManager.setIconAllowOverlap(true);
        symbolManager.setTextAllowOverlap(true);



        if (address != null) {
            SymbolOptions symbolOptions = new SymbolOptions().withLatLng(position)
                    .withTextField(address)
                    .withTextOffset(new Float[]{0f, 2f})
                    //.withTextFont(textFonts)
                    .withTextColor("#FFFFFF");
            symbolManager.create(symbolOptions);
            MapController.updateMarkerPosition(mapboxMap, position);
            MapController.setCameraPositionForCurrentActivity(mapboxMap, position);
        }

    }

    public static void updateMarkerPosition(MapboxMap mapboxMap, LatLng position) {
        GeoJsonSource geoJsonSource = mapboxMap.getStyle().getSourceAs(SOURCE_ID);
        if (geoJsonSource != null) {
            geoJsonSource.setGeoJson(Feature.fromGeometry(Point.fromLngLat(position.getLongitude(), position.getLatitude())));
        }
    }

    public static void updateMarkerPositionCollection(MapboxMap mapboxMap, List<Feature> symbolLayerIconFeatureList) {
        GeoJsonSource geoJsonSource = mapboxMap.getStyle().getSourceAs(SOURCE_ID);
        if (geoJsonSource != null) {
            geoJsonSource.setGeoJson(FeatureCollection.fromFeatures(symbolLayerIconFeatureList));
        }
    }

    public static Style.Builder initSymbolLayer() {
        return new Style.Builder().fromUri("mapbox://styles/nilsthie/ckobdkrj8103n17qsvn5852kn")// Add the SymbolLayer icon image to the map style
                .withSource(new GeoJsonSource(SOURCE_ID))
                .withLayer(new SymbolLayer(LAYER_ID, SOURCE_ID)
                        .withProperties(
                                iconAnchor(Property.ICON_ANCHOR_BOTTOM),
                                iconImage(ICON_ID),
                                iconAllowOverlap(true),
                                iconIgnorePlacement(true)
                        ));
    }

    public static void setCameraPositionForCurrentActivity(MapboxMap mapboxMap, LatLng latLngOfCurrentActivity) {
        CameraPosition position = new CameraPosition.Builder()
                .target(latLngOfCurrentActivity)
                .zoom(15)
                .tilt(0)
                .build();

        mapboxMap.setCameraPosition(position);
    }

    public static void enableLocationComponent(Context context, MapboxMap map, Style style) {
        LocationComponent locationComponent = map.getLocationComponent();
        locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(context, style).build());
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String [] {Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            Log.i("Permission: ", "granted");
            locationComponent.setLocationComponentEnabled(true);
        }
        LocationComponentOptions locationComponentOptions = LocationComponentOptions.builder(context)
                .pulseEnabled(true)
                .pulseFadeEnabled(true)
                .build();

        LocationComponentActivationOptions locationComponentActivationOptions = LocationComponentActivationOptions
                .builder(context, style)
                .locationComponentOptions(locationComponentOptions)
                .build();

        locationComponent.activateLocationComponent(locationComponentActivationOptions);
        locationComponent.setCameraMode(CameraMode.TRACKING);
        locationComponent.setRenderMode(RenderMode.COMPASS);
        locationComponent.getLastKnownLocation();
    }

    public static void resetLocation(Context context, MapboxMap map, Style style) {
        LocationComponent locationComponent = map.getLocationComponent();
        locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(context, style).build());
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String [] {Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            Toast.makeText((Activity) context, "Location Permission not granted!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("Permission: ", "granted");
            locationComponent.setLocationComponentEnabled(true);
        }
        LocationComponentOptions locationComponentOptions = LocationComponentOptions.builder(context)
                .pulseEnabled(true)
                .pulseFadeEnabled(true)
                .build();

        LocationComponentActivationOptions locationComponentActivationOptions = LocationComponentActivationOptions
                .builder(context, style)
                .locationComponentOptions(locationComponentOptions)
                .build();

        locationComponent.activateLocationComponent(locationComponentActivationOptions);
        locationComponent.setCameraMode(CameraMode.TRACKING);
        locationComponent.setRenderMode(RenderMode.COMPASS);
        locationComponent.getLastKnownLocation();
        Location lastKnownLocation = map.getLocationComponent().getLastKnownLocation();
        map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())));
    }

    public static void resetToMarker(Context context, MapboxMap map, Style style, LatLng position) {
        LocationComponent locationComponent = map.getLocationComponent();
        locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(context, style).build());
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String [] {Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            locationComponent.setLocationComponentEnabled(true);
        }
        LocationComponentOptions locationComponentOptions = LocationComponentOptions.builder(context)
                .pulseEnabled(true)
                .pulseFadeEnabled(true)
                .build();

        LocationComponentActivationOptions locationComponentActivationOptions = LocationComponentActivationOptions
                .builder(context, style)
                .locationComponentOptions(locationComponentOptions)
                .build();

        locationComponent.activateLocationComponent(locationComponentActivationOptions);
        locationComponent.setCameraMode(CameraMode.TRACKING);
        locationComponent.setRenderMode(RenderMode.COMPASS);
        map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(position.getLatitude(), position.getLongitude())));
    }
}
