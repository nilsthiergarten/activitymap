package de.cmlab.drm_app2.infrastructure.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Data
@Entity
public class Alarm {

    @PrimaryKey(autoGenerate = true)
    private long alarmId;

    private long dailyActivityDateId;

    @ColumnInfo(name = "wake_time")
    private String wakeTime;

    public Alarm(long dailyActivityDateId, String wakeTime) {
        this.wakeTime = wakeTime;
        this.dailyActivityDateId = dailyActivityDateId;
    }

    public long getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(long alarmId) {
        this.alarmId = alarmId;
    }

    public long getDailyActivityDateId() {
        return dailyActivityDateId;
    }

    public void setDailyActivityDateId(long dailyActivityDateId) {
        this.dailyActivityDateId = dailyActivityDateId;
    }

    public String getWakeTime() {
        return wakeTime;
    }

    public void setWakeTime(String wakeTime) {
        this.wakeTime = wakeTime;
    }
}
