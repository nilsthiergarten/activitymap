package de.cmlab.drm_app2.infrastructure.repository;

import android.app.Application;

import java.util.List;

import de.cmlab.drm_app2.infrastructure.dao.DailyActivityAndActivitiesAndAlarmDao;
import de.cmlab.drm_app2.infrastructure.database.AppDatabase;
import de.cmlab.drm_app2.infrastructure.entity.DailyActivityAndActivitiesAndAlarm;

public class DailyActivityAndActivitiesAndAlarmRepository {

    private DailyActivityAndActivitiesAndAlarmDao dailyActivityAndActivitiesAndAlarm;

    public DailyActivityAndActivitiesAndAlarmRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        this.dailyActivityAndActivitiesAndAlarm = db.dailyActivityAndActivitiesAndAlarmDao();
    }

    public List<DailyActivityAndActivitiesAndAlarm> getAll() {
        return this.dailyActivityAndActivitiesAndAlarm.getAll();
    }
}
