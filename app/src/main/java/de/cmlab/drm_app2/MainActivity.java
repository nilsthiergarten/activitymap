package de.cmlab.drm_app2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.cmlab.drm_app2.backgroundServices.alarm.AlarmJobService;
import de.cmlab.drm_app2.backgroundServices.alarm.AlarmService;
import de.cmlab.drm_app2.backgroundServices.gpsTracking.LocationJobService;
import de.cmlab.drm_app2.backgroundServices.gpsTracking.LocationService;
import de.cmlab.drm_app2.backgroundServices.reminder.ReminderJobService;
import de.cmlab.drm_app2.backgroundServices.reminder.ReminderService;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.Place;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorage;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorageImpl;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.Keys;
import de.cmlab.drm_app2.infrastructure.utils.MapController;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private final InMemoryStorage inMemoryStorage = InMemoryStorageImpl.getInstance();

    private MapView mapView;
    private MapboxMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityViewModel activityViewModel = new ActivityViewModel(this.getApplication());
        ActivityViewModel.setInstance(activityViewModel);

        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        setContentView(R.layout.activity_main);

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.getMapAsync(this);

        inMemoryStorage.store(Keys.SelectedDate, getCurrentDate());
        startAllBackgroundJobsIfNotAlreadyRunning();
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        map = mapboxMap;
        mapboxMap.setStyle(initSymbolLayer(), this::loadLocation);

        mapboxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(Style style) {
                MapController.enableLocationComponent(MainActivity.this,mapboxMap, style);
            }
        });
    }

    public void onCurrentLocationClick(View view) {
        ImageButton imageButton = findViewById(R.id.currentLocation);
        map.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(Style style) {
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MapController.resetLocation(MainActivity.this, map, map.getStyle());
                    }
                });
            }
        });
    }


    public void onPlusButtonClick(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    public void OnOverviewButtonClick(View view) {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    public void onSettingButtonClick(View view) {
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }

    private Style.Builder initSymbolLayer() {
        return MapController.initSymbolLayer()// Add the SymbolLayer icon image to the map style
                .withImage("ICON_ID", BitmapFactory.decodeResource(
                        this.getResources(), R.drawable.marker)); // Adding a GeoJson source for the SymbolLayer icons.
    }

    private void loadLocation(Style style) {
        MapController.drawAddressOnMap(mapView, map, null, null, style);
        new Thread(() -> {
            List<Feature> symbolLayerIconFeatureList = new ArrayList<>();

            LocalDate localDate = (LocalDate) InMemoryStorageImpl.getInstance().retrieve(Keys.SelectedDate);
            List<ActivityAndPlace> activityAndPlaceList = ActivityViewModel.getInstance().getActivitiesWithPlaceForLocalDate(localDate);

            for (ActivityAndPlace activityAndPlace : activityAndPlaceList) {
                Place place = activityAndPlace.getPlace();
                symbolLayerIconFeatureList.add(
                        Feature.fromGeometry(Point.fromLngLat(place.getLongitude(), place.getLatitude()))
                );
            }

            runOnUiThread(() -> MapController.updateMarkerPositionCollection(map, symbolLayerIconFeatureList));
        }, "LocationLoader-Thread").start();
    }

    private void startAllBackgroundJobsIfNotAlreadyRunning() {
        JobScheduler jobScheduler = (JobScheduler) this.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler.getAllPendingJobs().stream().noneMatch(job -> job.getId() == LocationJobService.JOB_ID)) {
            startForegroundService(new Intent(getApplicationContext(), LocationService.class));
        }

        if (jobScheduler.getAllPendingJobs().stream().noneMatch(job -> job.getId() == AlarmJobService.JOB_ID)) {
            startForegroundService(new Intent(getApplicationContext(), AlarmService.class));
        }

        if (jobScheduler.getAllPendingJobs().stream().noneMatch(job -> job.getId() == ReminderJobService.JOB_ID)) {
            startForegroundService(new Intent(getApplicationContext(), ReminderService.class));
        }
    }

    public void onDateClickerClick(View view) {
        DatePickerDialog.OnDateSetListener dateSetListener = (view1, year, month, day) -> {
            LocalDate localDateTime = LocalDate.of(year, month + 1, day);
            inMemoryStorage.store(Keys.SelectedDate, localDateTime);

            // Re-draw markers on map
            map.setStyle(initSymbolLayer(), this::loadLocation);
        };

        LocalDate currentDate = (LocalDate) inMemoryStorage.retrieve(Keys.SelectedDate);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, dateSetListener, currentDate.getYear(), currentDate.getMonthValue() - 1, currentDate.getDayOfMonth());
        datePickerDialog.show();

        int color = getResources().getColor(R.color.blue_500);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(color);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(color);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
    }

    private LocalDate getCurrentDate() {
        LocalDate localDate = LocalDate.now();
        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();

        return LocalDate.of(year, month, day);
    }
}
