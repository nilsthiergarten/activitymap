package de.cmlab.drm_app2;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.Alarm;
import de.cmlab.drm_app2.infrastructure.utils.MapController;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;
import de.cmlab.drm_app2.ui.RatedView;

public class ProfileDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String detailInfo = "profile-detail-info";

    private final ActivityViewModel activityViewModel = new ActivityViewModel(getApplication());
    private ActivityAndPlace currentActivityAndPlace;
    private MapView mapView;
    private MapboxMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail);

        Intent intent = getIntent();
        currentActivityAndPlace = (ActivityAndPlace) intent.getSerializableExtra(ProfileActivity.detailInfo);

        setActivityText();
        setRating();
        setAlarmText();

        mapView = findViewById(R.id.activity_profile_detail_mapView);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.map = mapboxMap;
        LatLng latLngOfCurrentActivity = new LatLng(currentActivityAndPlace.getPlace().getLatitude(), currentActivityAndPlace.getPlace().getLongitude());
        String address = currentActivityAndPlace.getPlace().getAddress();
        mapboxMap.setStyle(initSymbolLayer(), style -> MapController.drawAddressOnMap(mapView, mapboxMap, address, latLngOfCurrentActivity, style));

        ImageButton currentLocationButton = findViewById(R.id.currentLocation2);
        currentLocationButton.setVisibility(View.VISIBLE);

        currentLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapboxMap.getStyle(new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(Style style) {
                        MapController.resetToMarker(ProfileDetailActivity.this, mapboxMap, style,latLngOfCurrentActivity);
                    }
                });
            }
        });
    }
    private Style.Builder initSymbolLayer() {
        return MapController.initSymbolLayer()// Add the SymbolLayer icon image to the map style
                .withImage("ICON_ID", BitmapFactory.decodeResource(
                        this.getResources(), R.drawable.marker)); // Adding a GeoJson source for the SymbolLayer icons.
    }

    public void EditEvent(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra(detailInfo, currentActivityAndPlace);
        startActivity(intent);
    }

    private void setActivityText() {
        TextView startTextView = findViewById(R.id.activity_profile_detail_start_time);
        TextView endTextView = findViewById(R.id.activity_profile_detail_end_time);
        TextView descriptionTextView = findViewById(R.id.activity_profile_detail_description);

        Typeface typeface = getResources().getFont(R.font.quicksand_regular);
        Typeface quicksandSemibold = getResources().getFont(R.font.quicksand_semibold);

        setFont(startTextView, quicksandSemibold);
        setFont(endTextView, quicksandSemibold);
        setFont(descriptionTextView, typeface);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, d MMM \n HH:mm");

        LocalDateTime startDate = LocalDateTime.parse(currentActivityAndPlace.getActivity().getStartTime());
        LocalDateTime endDate = LocalDateTime.parse(currentActivityAndPlace.getActivity().getEndTime());

        startTextView.setText(startDate.format(formatter));
        endTextView.setText(endDate.format(formatter));

        String description = currentActivityAndPlace.getActivity().getDescription();
        if (description != null && !description.isEmpty()) {
            descriptionTextView.setText(description);
        } else {
            descriptionTextView.setText("Empty Description");
        }
    }

    private void setRating() {
        RatedView ratedView = findViewById(R.id.activity_profile_detail_ratingView);
        if (currentActivityAndPlace.getActivity().getRatingX() == 0) {
            ratedView.setRatingX(ratedView.getWidth()/2);
            ratedView.setRatingY(ratedView.getHeight()/2);
        } else {
            ratedView.setRatingX(currentActivityAndPlace.getActivity().getRatingX());
            ratedView.setRatingY(currentActivityAndPlace.getActivity().getRatingY());
        }

    }

    private void setFont(TextView text, Typeface typeface) {
        text.setTypeface(typeface);
        text.setTextColor(Color.WHITE);
    }

    private void setAlarmText() {
        TextView textView = findViewById(R.id.lastAlarm);
        new Thread(() -> {
            List<Alarm> alarmList = activityViewModel.alarmRepository.getAll();
            Log.i("AlarmList", "empty?" + alarmList.toString());
            if (alarmList != null && alarmList.isEmpty()){
                runOnUiThread(() -> textView.setVisibility(View.GONE));
            } else {
                Alarm lastAlarm = alarmList.get(alarmList.size() - 1);
                runOnUiThread(() -> textView.setText(lastAlarm.getWakeTime()));
            }

        }).start();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
