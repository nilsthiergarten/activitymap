package de.cmlab.drm_app2;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.material.snackbar.Snackbar;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.Alarm;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorageImpl;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.Keys;
import de.cmlab.drm_app2.infrastructure.viewmodel.ActivityViewModel;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class ProfileActivity extends AppCompatActivity {

    public static final String detailInfo = "detail-info";
    private final ActivityViewModel activityViewModel = new ActivityViewModel(getApplication());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        populateTimeline(savedInstanceState);
    }

    private void populateTimeline(Bundle savedInstanceState) {
        LinearLayout activityContainer = findViewById(R.id.activityContainer);
        activityContainer.removeAllViews();

        ActionBar.LayoutParams textLayoutParams = new ActionBar.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        textLayoutParams.setMargins(15, 8, 0, 8);

        ActionBar.LayoutParams cardViewLayoutParams = new ActionBar.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        cardViewLayoutParams.setMargins(0, 8, 0, 8);

        Typeface typeface = getResources().getFont(R.font.quicksand_regular);


        new Thread(() -> {
            LocalDate localDate = (LocalDate) InMemoryStorageImpl.getInstance().retrieve(Keys.SelectedDate);
            List<ActivityAndPlace> activityAndPlaceList = activityViewModel.getActivitiesWithPlaceForLocalDate(localDate);
            DateTimeFormatter activityHeadFormat = DateTimeFormatter.ofPattern("EEE, d MMM");
            DateTimeFormatter activityFormat = DateTimeFormatter.ofPattern("HH:mm");

            activityAndPlaceList.forEach(x -> {
                CardView cardView = new CardView(this);

                cardView.setRadius(12);
                cardView.setElevation(6);
                cardView.setLayoutParams(cardViewLayoutParams);
                cardView.setPadding(8, 8, 8, 8);
                cardView.setCardBackgroundColor(getResources().getColor(R.color.blue_500));
                cardView.setRadius(30);
                cardView.setOnClickListener(registerOnClick(x));
                cardView.setOnLongClickListener(registerOnLongClick(x, savedInstanceState));

                TextView textView = new TextView(this);

                LocalDateTime startDate = LocalDateTime.parse(x.getActivity().getStartTime());
                LocalDateTime endDate = LocalDateTime.parse(x.getActivity().getEndTime());

                textView.setText(String.format("%s\n%s - %s\n%s",
                        startDate.format(activityHeadFormat),
                        startDate.format(activityFormat),
                        endDate.format(activityFormat),
                        removeCountryFromAddress(x.getPlace().getAddress())));
                textView.setLayoutParams(textLayoutParams);
                textView.setTypeface(typeface);
                textView.setTextColor(Color.WHITE);


                runOnUiThread(() -> {
                    cardView.addView(textView);
                    activityContainer.addView(cardView);
                });
            });
        }).start();
    }

    /**
     * Takes an address of format like "An der Weberei 5, 96047 Bamberg, Germany"
     * and removes county from it by splitting at the ',' and removing the last element
     * and joining the remaining parts like ','
     *
     * @param address address of format like "An der Weberei 5, 96047 Bamberg, Germany"
     * @return address of format like "An der Weberei 5, 96047 Bamberg"
     */
    private String removeCountryFromAddress(String address) {
        String[] splittedAddress = address.split(",");
        return String.join(",", Arrays.copyOf(splittedAddress, splittedAddress.length - 1));
    }

    private View.OnClickListener registerOnClick(ActivityAndPlace activityAndPlace) {
        return x -> {
            Intent intent = new Intent(this, ProfileDetailActivity.class);
            intent.putExtra(detailInfo, activityAndPlace);
            startActivity(intent);
        };
    }

    private View.OnLongClickListener registerOnLongClick(ActivityAndPlace activityAndPlace, Bundle savedInstanceState) {
        return view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Do you want to delete this activity?")
                    .setPositiveButton(Html.fromHtml("<font color='#84A0EF'>Yes</font>"), (dialog, which) ->
                            new Thread(() -> {
                                activityViewModel.activityRepository.deleteActivity(activityAndPlace.getActivity());

                                runOnUiThread(() -> populateTimeline(savedInstanceState));

                                Snackbar snackbar = Snackbar.make(view, "Item was removed from the list.", Snackbar.LENGTH_LONG);
                                snackbar.setAction("UNDO", view1 ->
                                        new Thread(() -> {
                                            activityViewModel.activityRepository.insert(activityAndPlace.getActivity());
                                            runOnUiThread(() -> populateTimeline(savedInstanceState));
                                        }).start());
                                snackbar.show();
                            }).start())
                    .setNegativeButton(Html.fromHtml("<font color='#84A0EF'>No</font>"), (dialog, which) -> { /* nothing */ })
                    .show();
            return false;
        };
    }

}