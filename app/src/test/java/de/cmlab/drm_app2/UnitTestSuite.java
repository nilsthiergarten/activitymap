package de.cmlab.drm_app2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import de.cmlab.drm_app2.backgroundServices.gpsTracking.LocationAlgorithmTest;
import de.cmlab.drm_app2.sensors.gps.PositionServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ExampleUnitTest.class,
        PositionServiceTest.class,
        LocationAlgorithmTest.class
})
public class UnitTestSuite {
}
