package de.cmlab.drm_app2.backgroundServices.gpsTracking;

import android.location.Location;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import de.cmlab.drm_app2.infrastructure.entity.Activity;
import de.cmlab.drm_app2.infrastructure.entity.ActivityAndPlace;
import de.cmlab.drm_app2.infrastructure.entity.Place;
import de.cmlab.drm_app2.sensors.gps.PositionService;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownAddressesException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownLastLocationException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

// We need to run all tests with synchronized keyword
// As all tests manipulate the cache, we need to ensure each tests runs independent of each other
public class LocationAlgorithmTest extends LocationAlgorithm {

    Location workPlace = mock(Location.class);
    Location notWorkPlace = mock(Location.class);

    public LocationAlgorithmTest() throws UnknownAddressesException, UnknownLastLocationException, IOException {
        when(workPlace.getLatitude()).thenReturn(49.903244);
        when(workPlace.getLongitude()).thenReturn(10.869845);

        when(notWorkPlace.getLatitude()).thenReturn(0.0);
        when(notWorkPlace.getLongitude()).thenReturn(0.0);

        super.positionService = mock(PositionService.class);
        when(positionService.getAddressUsingLocationAsString(workPlace)).thenReturn("An der Weberei 5, 96047 Bamberg, Germany");
        when(positionService.getAddressUsingLocationAsString(notWorkPlace)).thenReturn("Somewhere else");
    }

    // Activity goes as following:
    // We are in the office for 8 hours and then leave
    // When we leave the office, we should receive the activity and then the cache shall be empty
    @Test
    public synchronized void EmptyCacheThenActivityThenEmptyCache() throws UnknownAddressesException, IOException, UnknownLastLocationException {
        activityCache = new ArrayList<>();

        LocalDateTime workTimeStartTime = LocalDateTime.of(2021, 1, 1, 8, 0);
        LocalDateTime workTimeIntermediate1Time = LocalDateTime.of(2021, 1, 1, 10, 0);
        LocalDateTime workTimeIntermediate2Time = LocalDateTime.of(2021, 1, 1, 12, 0);
        LocalDateTime workTimeIntermediate3Time = LocalDateTime.of(2021, 1, 1, 14, 0);

        LocalDateTime workTimeEndTime = LocalDateTime.of(2021, 1, 1, 16, 0);
        LocalDateTime afterWorkTime = LocalDateTime.of(2021, 1, 1, 18, 0);

        Activity expectedActivity = new Activity(workTimeStartTime.toString(), workTimeEndTime.toString(), "", 0, 0);
        Place expectedPlace = new Place("An der Weberei 5, 96047 Bamberg, Germany", 49.9032, 10.8698);
        ActivityAndPlace expected = new ActivityAndPlace(expectedActivity, expectedPlace);

        Assert.assertEquals(0, activityCache.size());

        Optional<ActivityAndPlace> workTimeStart = addPoint(workPlace, workTimeStartTime);
        Optional<ActivityAndPlace> workTimeIntermediate1 = addPoint(workPlace, workTimeIntermediate1Time);
        Optional<ActivityAndPlace> workTimeIntermediate2 = addPoint(workPlace, workTimeIntermediate2Time);
        Optional<ActivityAndPlace> workTimeIntermediate3 = addPoint(workPlace, workTimeIntermediate3Time);
        Optional<ActivityAndPlace> workTimeEnd = addPoint(workPlace, workTimeEndTime);
        Optional<ActivityAndPlace> afterWork = addPoint(notWorkPlace, afterWorkTime);

        Assert.assertEquals(0, activityCache.size());

        Assert.assertFalse(workTimeStart.isPresent());
        Assert.assertFalse(workTimeIntermediate1.isPresent());
        Assert.assertFalse(workTimeIntermediate2.isPresent());
        Assert.assertFalse(workTimeIntermediate3.isPresent());
        Assert.assertFalse(workTimeEnd.isPresent());
        Assert.assertTrue(afterWork.isPresent());

        Assert.assertEquals(expected.activity, afterWork.get().activity);
        Assert.assertEquals(expected.place, afterWork.get().place);
    }

    // If we are always moving and never stopping, no activity shall be entered
    @Test
    public synchronized void NoActivityAtAll() throws UnknownAddressesException, IOException, UnknownLastLocationException {
        activityCache = new ArrayList<>();

        Assert.assertEquals(0, activityCache.size());

        int dayCounter = 0;
        for (int i = 0; i < 100; i++) {
            if (dayCounter % 23 == 0) dayCounter++;
            LocalDateTime time = LocalDateTime.of(2021, 1, dayCounter, i % 23, 0);
            Location location = mock(Location.class);
            when(positionService.getAddressUsingLocationAsString(location)).thenReturn(Integer.toString(i));
            when(location.getLongitude()).thenReturn((double) i);

            Optional<ActivityAndPlace> result = addPoint(location, time);
            Assert.assertFalse(result.isPresent());
        }

        Assert.assertEquals(1, activityCache.size());
    }
}
