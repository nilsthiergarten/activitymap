package de.cmlab.drm_app2.sensors.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;

import androidx.core.app.ActivityCompat;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorage;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.InMemoryStorageImpl;
import de.cmlab.drm_app2.infrastructure.inMemoryStorage.Keys;
import de.cmlab.drm_app2.sensors.gps.exceptions.NoGPSPermissionException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownAddressesException;
import de.cmlab.drm_app2.sensors.gps.exceptions.UnknownLastLocationException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PositionServiceTest {

    @Test
    public void returnAddressWhenAvailable() throws IOException, NoGPSPermissionException, UnknownAddressesException, UnknownLastLocationException {
        // Arrange
        final double LATITUDE = 39.929475000000004;
        final double LONGITUDE = -75.22888833333333;

        Address expected = new Address(Locale.getDefault());
        expected.setAddressLine(0, "6120 Woodland Ave, Philadelphia, PA 19142, USA");

        ArrayList<Address> addresses = new ArrayList<>();
        addresses.add(expected);

        InMemoryStorage inMemoryStorage = InMemoryStorageImpl.getInstance();
        Context context = mock(Context.class);
        LocationManager locationManager = mock(LocationManager.class);
        Geocoder geocoder = mock(Geocoder.class);
        Location location = mock(Location.class);

        inMemoryStorage.store(Keys.Location, location);
        when(geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)).thenReturn(addresses);
        when(location.getLatitude()).thenReturn(LATITUDE);
        when(location.getLongitude()).thenReturn(LONGITUDE);
        when(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)).thenReturn(location);

        // Act

        PositionService positionService = new PositionServiceImpl(context, geocoder, inMemoryStorage);
        Address actual = positionService.getCurrentAddressUsingGPS();

        // Assert
        Assert.assertEquals(
                expected.getAddressLine(0),
                actual.getAddressLine(0));
    }

    @Test(expected = NoGPSPermissionException.class)
    public void noPermission() throws NoGPSPermissionException {
        // Arrange
        Context context = mock(Context.class);
        PositionService positionService = new PositionServiceImpl(context, null, null);

        when(ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION))
                .thenReturn(PackageManager.PERMISSION_DENIED);

        // Act
        positionService.startGPSTracking();
    }

    @Test(expected = UnknownLastLocationException.class)
    public void noLocationAvailable() throws IOException, UnknownLastLocationException, UnknownAddressesException {
        // Arrange
        InMemoryStorage inMemoryStorage = InMemoryStorageImpl.getInstance();
        PositionService positionService = new PositionServiceImpl(null, null, inMemoryStorage);

        // Act
        positionService.getCurrentAddressUsingGPS();
    }

    @Test(expected = UnknownAddressesException.class)
    public void noAddressAvailable() throws IOException, UnknownAddressesException, UnknownLastLocationException {
        // Arrange

        // this is somewhere in the atlantic ocean
        final double LATITUDE = 46.238483;
        final double LONGITUDE = -26.177083;

        Context context = mock(Context.class);
        Geocoder geocoder = mock(Geocoder.class);
        Location location = mock(Location.class);
        InMemoryStorage inMemoryStorage = InMemoryStorageImpl.getInstance();
        PositionService positionService = new PositionServiceImpl(context, geocoder, inMemoryStorage);

        when(location.getLatitude()).thenReturn(LATITUDE);
        when(location.getLongitude()).thenReturn(LONGITUDE);

        inMemoryStorage.store(Keys.Location, location);
        when(geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)).thenReturn(null);

        // Act
        positionService.getCurrentAddressUsingGPS();
    }

}
